from flask import Flask, render_template

from system_hooks import network, system

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('static/index.html', hostname=network.hostname(), uptime=system.uptime(), processes=system.top())

@app.route('/host')
def host():
    return network.hostname()

@app.route('/uptime')
def uptime():
    return system.uptime()
