import os

def uptime():
    return os.popen('uptime -p').read()[:-1]

def top():
    return os.popen('top -n1 -b | grep USER -A5').read()[:-1]